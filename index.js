
document.addEventListener("keydown", function (event) {
    console.log(event);  
    document.body.innerHTML = `
    <div class="result">
        &nbsp;&nbsp;&nbsp;
        <b>which: ${event.which}</b>
        <br>&nbsp;
        <b>keyCode: ${event.keyCode}</b>
        <br>&nbsp;&nbsp;&nbsp;
        <b>shiftKey: ${event.shiftKey}</b> 
        </br>&nbsp;&nbsp;&nbsp;&nbsp;
        <b>altKey: ${event.altKey}</b> 
        <br>&nbsp;&nbsp;&nbsp;
        <b>ctrlKey:${event.ctrlKey}</b> 
        <br>&nbsp;&nbsp;
        <b>metaKey:${event.metaKey}</b> 
        <br>&nbsp;&nbsp;
        <b>key:${event.key}</b>
        <br />
        <button id="refBtn">refresh</button>
    </div>`;
  });

  document.getElementById("refBtn");  
  document.addEventListener("click", function () {
    window.location.reload();
});